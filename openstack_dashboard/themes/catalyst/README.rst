=============================
Checklist for Horizon Upgrade
=============================

Catalyst Cloud dashboard is based on OpenStack Horizon with customized
theme and some other patches. Here are some check points after the upgrade
to make sure everything is correct.

Check points
------------

* Make sure all the private functional patches work as expected.
* Make sure all the plugins can display well.

  * Billing panel
  * Support (RT) panel
  * Adjutant Panel

* Pay more attention if there is a new plugin
* Make sure those error pages work well which are easy to be ignored
