// grunt is used only in development (i.e. this file does NOT need serving anywhere),
// for regenerating sprite sheets using https://github.com/Ensighten/grunt-spritesmith

// to install the dependancies to run this (i.e. if you want to regenerate the sprite
// sheets) on your dev instance do the following IN THE FOLDER THAT CONTAINS THIS FILE:

// sudo apt-get install -y software-properties-common python-software-properties python g++ make
// sudo add-apt-repository ppa:chris-lea/node.js
// sudo apt-get update
// sudo apt-get install nodejs
// npm install grunt
// sudo npm install -g grunt-cli

// and then to regenerate the sprite file do:

// grunt sprite

module.exports = function (grunt) {
  // Configure grunt
  grunt.initConfig({
    sprite:{
      all: {
        src: 'icons/*.png',
        dest: '../static/img/icons.png',
        destCss: '../static/_icon-sprite.scss',
        imgPath: '/static/themes/catalyst/img/icons.png',
        padding: 20,
        algorithm: 'top-down',
        cssVarMap: function (sprite) {
          sprite.name = 'icon-' + sprite.name;
        }
      }
    }
  });

  // Load in `grunt-spritesmith`
  grunt.loadNpmTasks('grunt-spritesmith');
}
